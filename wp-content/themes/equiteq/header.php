<?php
global $post, $have_bg;

echo get_the_title($post->post_parent);

$body_height = null;
$haveBg = array('industry', 'expert', 'resource');

if (in_array(get_post_type(), $haveBg)):
    $the_bar = (!is_user_logged_in()) ? '' : '-bar';
    $body_height = 'bodyTop' . $the_bar;
    $have_bg = 'have-bg';
endif;

?>

<!DOCTYPE html>
<html <?php language_attributes();?>>

<head>
<style>
        body {
            margin: 0;
            padding: 0;
        }
        header {
            display: flex;
            align-items: center;
            background-color: black;
            color: white;
            height: 50px;
        }
        .logo {
            flex: 1;
            padding-left: 20px;
        }
        .logo img {
            margin-top: 20px;
            height: 50px; 
        }
        nav {
            flex: 1;
            text-align: right;
            padding-right: 20px;
        }
        nav ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }
        nav li {
            display: inline-block;
            margin-left: 20px;
        }
        nav li:first-child {
            margin-left: 0;
        }
        nav li a {
            color: white;
            text-decoration: none;
            font-size: 16px;
        }
    </style>
        <header>
        <div class="logo">
        <img src="<?php echo get_template_directory_uri() ?>/images/svg/Equitec_Logo_ReverseColour_RGB-2.svg" alt="logo"        </div>
        </div>
        <nav>
            <ul>
                <li><a href="#">Services</a></li>
                <li><a href="#">Sectors</a></li>
                <li><a href="#">Deals</a></li>
                <li><a href="">Resources</a></li>
                <li><a href="#">Our Team</a></li>
                <li><a href="">About</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </nav>
    </header>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri() . '/images/favicon.ico' ?>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css">

    <?php wp_head()?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0099/9049.js" async="async"></script>
</head>

<body <?php body_class($body_height);?>>

<?php get_template_part('template-parts/navigation/navigation', 'top');?>

<div>